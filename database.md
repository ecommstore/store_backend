### Database Design
    AttributeName
        - _id
        - name
        
    AttributeValue
        - id
        - attr_name_id_fk
        - 

    AttributeProduct
        - id
        - 

    Product
        - id
        - brand
        - title

    VariantAttributeValue
        - id
        - 


    Variant
        - id
        - product_id
        - price
        
    
-- product
{
    id: 1,
    brand: "adidas",
    title: Camiseta Dry Sport 
}

-- variant
{
    id: 1
    product_id: 1


}
-- attr_value
{
    [
        {
            id: 1
            value: small
        },
        {
            id: 2
            value: medium
        },
        {
            id: 3
            value: large
        },
        {
            id: 4
            value: x-large
        }
    ]
   
}
-- variant_attr_value
{
    id: 1
    variant_id: 1
    attr_value_id: 1
}