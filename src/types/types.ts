import { ResponseCodes } from "../helper/http_codes";

export interface IResponseResult {
    code: ResponseCodes;
    message: string;
    data?: unknown;
}
