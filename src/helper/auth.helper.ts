import jwt from 'jsonwebtoken';
import { HttpCodes } from './http_codes';
import e from 'express';
import debug from 'debug';
import getSecretKey from './secret';

const LOG = debug('debug');

interface JsonMessage {
    code: number;
    message: string;
    items?: [];
}
/**
 * Middleware to check the token validity in the communication btw client & server
 * @param req 
 * @param res 
 * @param next This will be called if the validation success
 */
const checkAuth = (request: e.Request, response: e.Response, next: e.NextFunction) : void => {
    // if authorization 'TOKEN' doesn't exist in the header, 
    // should not allow the connection to the Backend

    if (request.headers.authorization === undefined) {
        const msg: JsonMessage = {code: HttpCodes.UNAUTHORIZED, message: 'Bad Authorization'};
        response.status(msg.code).json(msg);
        return;
    }
    // 'Bearer <token>'
    const token = request.headers['authorization'].split(' ') || [];
    if(token.length < 2 ){
        const msg: JsonMessage = {code: HttpCodes.UNAUTHORIZED, message: 'Token format is incorrect. should be like "Bearer <token>"'};
        response.status(msg.code).send(msg);
        return;
    }
    try{
        if( jwt.verify(token[1], getSecretKey()) ) {
            LOG('Token is valid')
            next(); // Let the request continue in the chain...
        }
        else {
            const msg: JsonMessage = {code: HttpCodes.UNAUTHORIZED, message: 'Token is correct parsed But, is not valid or has expired!'};
            response.status(msg.code).send(msg);
            return;
        }
    } catch(error) {
        LOG(`Error validating token ${error}`);
        const msg: JsonMessage = {code: HttpCodes.INTERNAL_SERVER_ERROR, message: `${error}`};
        response.status(msg.code).send(msg);
        return;
    }
}

export default checkAuth;