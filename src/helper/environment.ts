import dotenv from 'dotenv';

const setupEnvironment = () : void => {
    console.log(`Current environment is: ${process.env.NODE_ENV}`);
    
    if(process.env.NODE_ENV === 'development'){
        dotenv.config();
        process.env.DEBUG='server,database,user:model,debug,auth:controller,product:controller,promise:await:to';
    }
    else if(process.env.NODE_ENV === 'test')
    {
        dotenv.config();
        process.env.DEBUG = 'test:messages';

    }
    else if(process.env.NODE_ENV === 'production'){
        dotenv.config();
    }
    else
        throw new Error(`NODE_ENV environment variable undefined`);
}

export default setupEnvironment;
