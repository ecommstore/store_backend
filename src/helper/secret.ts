const getSecretKey = () : string => {
    return process.env.JWT_SECRET_KEY || "";
}

export default getSecretKey;