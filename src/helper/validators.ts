export default class Validator {
    
    static email( email: string ) : boolean {
        const regex = new RegExp(/^[A-Za-z]/);
        return regex.test(email);
    }

    static username( username: string ) : boolean {
        const regex = new RegExp(/^[A-Za-z]/);
        return regex.test(username);
    }
}