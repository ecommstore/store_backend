/**
 * Status codes to set in Response to client
 */
export enum ResponseCodes {
    OK = 200,
    USER_NOT_FOUND = 450,
    ERROR = 451,
    PRODUCT_NOT_FOUND = 452
}
export enum HttpCodes {
    OK = 200,
    UNAUTHORIZED = 401,
    NOT_FOUND = 404,
    INTERNAL_SERVER_ERROR = 500
}
// export default HttpCodes;