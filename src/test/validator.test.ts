import Validator from "../helper/validators";
import { expect } from "chai";
import debug from 'debug';

const LOG = debug('test:messages');

describe('Test Validation of Fields from requests', function() {
    it('Validate Username', function(done){

        expect(Validator.username('')).to.be.false;
        expect(Validator.username('efrainastudillo')).to.be.true;
        
        done();
    });
});