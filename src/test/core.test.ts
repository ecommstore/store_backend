import chai, { expect } from "chai";
import chaiHttp from 'chai-http';
import app from "../main";
import db from '../services/database';

import { HttpCodes, ResponseCodes } from "../helper/http_codes";
import debug from 'debug';
const LOG = debug('test:messages');

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('Test Server', function() {

  before('Connect to database',  function(done) {
    db.connect()
    .then( () => {
      LOG(`Database is connected`);
      done();
    })
    .catch(err => done(err));
  });

  after('Cleaning...', function(done) {
    db.clear('users');
    db.disconnect();

    LOG('Cleaning...');
    LOG('Disconnecting...');
    // app.shutdown();
    done();
  });


  it('Database is connected?', function(done) {
    expect(db.isConnected()).to.be.true;
    done();
  });

  it('GET / ', function(done) {
    chai.request(app.app)
    .get('/')
    .set('authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImVmcmFpbmFzdHVkaWxsbyIsImlkIjoiNWU0ZGIxNWU3MTgyMTc1ZjlmY2U2M2FkIiwiaWF0IjoxNTg5MzI4NjUxLCJleHAiOjE1ODkzMzIyNTF9.tLjFzfMaWd82yN88YPJ6UOny7Zw02RJPSEsBgLm0eGE')
    .end( (err, res) => {
      if(err)
        done(err);
      res.should.have.status(HttpCodes.INTERNAL_SERVER_ERROR);// token expirred
      LOG(res.text);
      
      done()
    });
  });

  it('POST /login  =>  Expected User NOT found', function(done) {
    chai.request(app.app)
    .post('/login')
    .type('json')
    .send({
      'username': "efrainastudillo",
      'password': 'Icqanet1234%'
    })
    .end( (err, res) => {
      if(err)
        done(err);
      
      expect(res).to.have.status(HttpCodes.OK);
      expect(res.body).to.not.be.null;
      expect( res.body.code ).to.be.equal(ResponseCodes.USER_NOT_FOUND);
      // expect(res.body.message).equal('User found!');
      expect(res.body.data).to.be.undefined;
      LOG(res.body.errorMsg);
      done();
    });
  });

  it('POST /signup  => Create a new user in database', function(done) {
    chai.request(app.app)
    .post('/signup')
    .type('json')
    .send({
      username: 'efrainastudillo',
      email: 'efrain.astudillo58@gmail.com',
      password: 'Icqanet1234%'
    })
    .end( (err, res) => {
      if(err)
        done(err);

      expect(res).to.have.status(HttpCodes.OK);
      expect(res.body.code).to.be.equal(ResponseCodes.OK);
      expect(res.body).to.not.be.null;
      expect(res.body.message).to.be.equal('User created successfully');
      expect( res.body.data ).to.not.be.undefined;
      LOG(res.body.data);
      done();
    });
    
  });


  // Test a login again. Now with the user just created before
  it('POST /login  => Login expected', function(done){
    chai.request(app.app)
    .post('/login')
    .type('json')
    .send({
      username: 'efrainastudillo',
      password: 'Icqanet1234%'
    })
    .end( (err , res) => {
      if(err)
        done(err);

      expect(res).to.have.status(HttpCodes.OK);
      expect(res.body.code).to.be.equal(ResponseCodes.OK);
      expect(res.body.data).to.be.not.null;
      expect(res.body.data).to.be.not.undefined;

      done();
    });
  });
});