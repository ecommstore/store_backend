// it imports an instance already. No need to create another one
import db from '../services/database';
import { MongoClient } from 'mongodb';

interface Product {
    title: string,
    description: string,
    price: number,
    image: string,
}

const data: Product [] = [ 
    { 
        title: 'Nest Thermostat E',
        description: 'Backlit Display,Battery Back-Up,Built-in Wi-Fi,Daylight Savings Time Ready,Lockable,Touch Screen,Vacation Mode',
        image: '/img/nest-thermotast-e.jpg',
        price: 169.00
    }, 
    { 
        title: 'Arlo Pro 2 1080p Wire-Free Security 2 Camera System',
        description: '1080p HD video with sharper and brighter details. Sound and Advanced Motion Detection. Wire-free, weather resistant, and rechargeable',
        image: '/img/arlo.jpg',
        price: 199.99
    },
    { 
        title: 'Smart Garage Door Opener',
        description: 'Chamberlain 1-1/4 HP Equivalent Ultra-Quiet Belt Drive Smart Garage Door Opener with Battery Backup',
        image: '/img/door.jpg',
        price: 245.40
    },
    { 
        title: 'Pearl Stand Mixer',
        description: 'Professional 600 Series 6 Qt. 10-Speed Nickel Pearl Stand Mixer',
        image: '/img/batidora.jpg',
        price: 499.99
    },
    { 
        title: 'TP-Link router Wireless',
        description: 'TP-LINK AC1200 Wireless Dual Band Gigabit Router',
        image: '/img/wireless.jpg',
        price: 49.99
    }
];

const populate = async ( _client: MongoClient , _data: Product[] ) => {
    console.log(`Adding some fixtures data to our database: ${_client}`);
    const res = await _client.db('store').collection('products').deleteMany({});
    if(res.result.ok)
        console.log(`Documents deleted ${res.result.n}`);
        
    _client.db('store').collection('products').insertMany( _data )
    .then( (_result) => {
        console.log(`Data inserted successfully. ${_result.result.n} documents were inserted`);
    })
    .catch( err => {
        console.log(`Error adding fixtures products to the database ${err}`);
    });
}

db.connect()
.then( ( _client: MongoClient) => {
    populate(_client, data );
})
.catch( _error => {
    console.log(`Error connecting to DB: ${_error}`);
});