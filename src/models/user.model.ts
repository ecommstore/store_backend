import db from "../services/database";
import debug from 'debug';
import * as jwt from 'jsonwebtoken';
import getSecretKey from "../helper/secret";
import * as bcrypt from 'bcrypt';
import { IUser, IReturnStatus } from "./interfaces/user.interface";
import to from 'await-to-js';
import { ObjectId } from "mongodb";

const LOG = debug('user:model');


export default class User implements IUser {
    private readonly SALT_PASSWORD = 10;

    constructor(
        public username: string,
        public password: string,
        public email?: string,
        public token?: string,
        public created_at?: Date,
        public updated_at?: Date
        )
    {

    }


    getUserById( _id: string ) {}

    /**
     * TODO: create test fot this method
     * @param _username 
     */
    static async getUserByUsername(_username: string) : Promise<User> {
        const [errFind, user] = await to( db.getDB().collection('users').findOne<User>({username: _username}) );
        if(errFind || !user)
            throw new Error(`User not found ${errFind}`);

        return user!;
    }
    /**
     * Will look for a user identified by username and password.
     * If it's found, will create a new token for this
     * TODO: create test for this method
     * @param _username should be unique
     * @param _password hashed password
     * @returns IReturnStatus. status will be true if a user is found. False otherwise
     * @throws an Error. User not found or token could not be generated
     */
     async validate() : Promise<IReturnStatus> {
        const [errFind, user]= await to(db.getDB().collection('users')
                                    .findOne<User>({username: this.username}));
        if(errFind) // if there is error
            throw errFind;
        if(!user)
            return {status: false, errorMsg: 'User NOT found'};
        
        const [errCompare, match] = await to(bcrypt.compare(this.password, user.password));
        if(errCompare)
            throw errCompare;
        if(!match)
            return {status: false, errorMsg: "User's password doesn't match"};
        
        // ? it seems like sign never throws an error, so we can safely leave as it is withouth manage any error
        // !I have to test this
        const token = jwt.sign(`${this.username}.${ this.password}`, getSecretKey());
            
        if(!token)
            throw new Error('Can not generate a new Token');
        
        // TODO: I have to update paramater of this instance whith the data of user returned
        return {status: true, data:token};

        //  this.username =  user.username;
    }
    /**
     * TODO: create test for this method
     * Create a new user if not registered in the database. This will set the created_at field and the _id object.
     * @returns ObjectId the id of the user just inserted in the database
     * @throws an Error any or if a user is already in database.
     */
    async create() : Promise<ObjectId> {
        //check if user to be create is already in database
        const [errFind, user] = await to(db.getDB()
                                        .collection('users')
                                        .findOne<User>({username: this.username}));
        if(errFind) // error
            throw errFind;
        if(user) // user already exists, can not create another one similar
            // return {status: false, errorMsg: 'User already exist', data: user};
            throw new Error('User already exists!');

        // TODO: THis line can be ommited. Only using this.SALT_PASSWORD with hash function will work
        const [errSalt, salt] = await to(bcrypt.genSalt(this.SALT_PASSWORD));
        let res: [Error|null, string|undefined];
        if(errSalt) // error 
            res = await to(bcrypt.hash(this.password, this.SALT_PASSWORD)); // with a number
        else
            res = await to(bcrypt.hash(this.password, salt!));// or with a string
        
        if( res.length > 1 ) // Error creating hash password
        {
            if( res[0] )
                throw res[0];
            if( !res[1] )
                throw new Error("Couldn't create hash for password");
        }

        const [errInsert, resultInsert] = await to(
            db.getDB().collection<IUser>('users').insertOne({
            username: this.username,
            password: res[1]!,
            email: this.email,
            created_at: new Date(),
            updated_at: new Date()
        }));

        if(errInsert)
            throw errInsert;
        if( resultInsert?.insertedCount !== 1 )
            throw new Error('User was not saved!');

        return resultInsert.insertedId;
    }
    /**
     * Save a User in the database. This method will indeed update data. 
     * !WE must consider to update explicit email and password here
     * TODO: must test this method
     * @returns IReturnStatus
     * @throws an Error if something happend while saving a user in database
     */
    async save() : Promise<IReturnStatus> {
        const [errInsert, saved] = await to( db.getDB()
                                .collection<IUser>('users')
                                .insertOne( {username: this.username, password: this.password, email: this.email, updated_at: new Date()}));
        if( errInsert ) // error
            throw errInsert;

        // TODO: make sure that saved always return something, otherwise insertedCount will throw an error
        LOG(`User saved ${saved?.insertedCount}`);
        return {status: true, data: saved};
    }


}