import { ObjectId } from "mongodb";

export interface IUser {
    _id?: ObjectId;
    username: string;
    password: string;
    email?: string;
    created_at?: Date;
    updated_at?: Date;
    roles?: Array<string>;
}
export interface IReturnStatus {
    status: boolean,
    data?: any,
    errorMsg?: string
}

export interface ISession {
    _id: string;
    user_id: string;
    role: number
    token: string;
}