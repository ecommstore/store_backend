import express from 'express';

import setup from './helper/environment';
setup();// initialize the environment variables

import debug from 'debug';
import path from 'path';
import bodyParser from 'body-parser';
import router from './routes';
import database from './services/database';
import {Server} from 'http';

// import util from 'util';
// console.log(`Inpsect process.env variable: ${util.inspect(process.env) }`);
const LOG = debug('server');

const app = express();
const PORT = process.env.PORT || 8080;

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    // res.header("Http-Method", "GET,POST,PUT, DELETE, PATCH")
    next();
  });
// Configure app to user bodyParser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Configure app to use EJS templates
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.static('public'));

app.use('/', router );


let server: Server;
database.connect()
.then(() => {
    server = app.listen(PORT, () => {
        LOG(`Example app listening on port ${PORT} : http://localhost:${PORT}`);
    });
})
.catch(error => {
    LOG(error);
});

const shutdown = () => {
    LOG('Database is disconnecting...');
    database.disconnect();
    LOG('Server is shutting down...');
    server.close();
    process.exit(0);
}

process.on('SIGINT', shutdown ); // callback when CTRL + C is hit
// export default db;
export default { app, shutdown };
