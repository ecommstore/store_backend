import debug from 'debug';
import { MongoClient, Db } from "mongodb";
// import setup from '../helper/config_env';
import to from 'await-to-js';

// setup();

const {
    MONGO_USERNAME,
    MONGO_PASSWORD,
    // MONGO_HOSTNAME,
    // MONGO_PORT,
    MONGO_DB
} = process.env;

const LOG = debug('database');

class IDataBase{
  private readonly URL = `mongodb+srv://${MONGO_USERNAME}:${MONGO_PASSWORD}@cluster0-0ys9x.mongodb.net/${MONGO_DB}?retryWrites=true&w=majority`;
  private readonly DATABASE_NAME = MONGO_DB || 'store';
  private clientDB: MongoClient;

  private database!: Db;
  // options how to create a database
  private readonly options = {
    useNewUrlParser: true,
    connectTimeoutMS: 10000,
    useUnifiedTopology: true
  };

  /**
   * Constructor
   */
  constructor(){
    if( MONGO_USERNAME === undefined || MONGO_PASSWORD === undefined )
      throw new Error('Mongo username & password undefined');

    this.clientDB = new MongoClient(this.URL, this.options);
  }
  /**
   * @return true if database is already connected, otherwise false
   */
  isConnected() : boolean {
    return this.clientDB.isConnected();
  }
  /**
   * Connect to database
   * ? Check if client.db(db_name) is working...
   */
  async connect() : Promise<MongoClient> {
    if(!this.isConnected()) {
      const [errConnection, client] = await to(this.clientDB.connect());
      if(errConnection)
        throw errConnection;
      if(!client)
        throw new Error('Can not retrieve a successful connection');

      this.database = client.db(this.DATABASE_NAME);
      LOG('Database connected!!')
      return client;
    }
    return this.clientDB;
  }

  /**
   * Remove all documents in a collection identified by collectionName 
   * !Has to fix the promise being returned. Use to() utility
   * 
   * @param collectionName Name of the collection to be removed
   * @throws an Error if this.database is null or can not drop a collecion
   */
  async clear(collectionName: string) : Promise<void> {
    if(this.database && this.isConnected() ){
      const [errDrop] = await to(this.database.collection(collectionName).drop());
      if(errDrop)
        throw errDrop;
      return;
    }
    throw new Error('Can not remove a collection because Database is null or it is NOT connected.');
  }

  /**
   * Disconnect database
   * @return true if disconnection was successuful, false otherwise
   * @throws an Error if couldn't close database
   */
  async disconnect() : Promise<void> {
    LOG('Disconnecting database...');
    const [errClose] = await to(this.clientDB.close(true));
    if(errClose)
      throw errClose;
    return;
  }

  /**
   * @return database object to create queries like find, InsertOne, etc
   */
  getDB() : Db {
    return this.database;
  }
}

//  I do this because I want to have only a single instance of this object in cache.
//  I need to keep it an eye to invalidate the cache if needed to prevent issues
export default new IDataBase();