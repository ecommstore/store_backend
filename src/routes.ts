import e from "express";
import UserController from "./controllers/user.controller";
import AuthController from "./controllers/auth.controller";
import checkAuth from "./helper/auth.helper";
import ProductController from "./controllers/product.controller";

const router = e.Router();

//UserController
router.get('/users/:id', checkAuth, UserController.getUserById );
router.post('/users/create', UserController.addUser );

//AuthController
router.post('/login', AuthController.login);
router.post('/signup', AuthController.signup);
router.get('/', checkAuth, AuthController.index);

router.get('/products', checkAuth, ProductController.getProducts);
router.get('products/:id', checkAuth, ProductController.getProductById);

export default router;