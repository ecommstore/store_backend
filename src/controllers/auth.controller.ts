import e, { response } from "express";
import User from "../models/user.model";
import { IResponseResult } from "../types/types";
import { HttpCodes, ResponseCodes} from "../helper/http_codes";
import debug from 'debug';
import { IReturnStatus } from "../models/interfaces/user.interface";
import { ObjectId } from "mongodb";

const LOG = debug('auth:controller');

class AuthController {
    private readonly NAME = 'AUTH_CONTROLLER';

    constructor(){}

    static async login(_request: e.Request, _response: e.Response<IResponseResult>) : Promise<any> {
        const { username, password } = _request.body;
        const user = new User( username, password );
        // will return 'ok' = false if user not found
        // otherwise, ok=true and 'token' will contain the token recently created.
        let res: IReturnStatus;
        try{
            res = await user.validate();
        } catch( error ){
            return _response.status(HttpCodes.OK).json({code: ResponseCodes.ERROR, message: `Error: ${error}` });
        };

        if( !res.status ) // if status is false check for errorMsg
            return _response.status(HttpCodes.OK).json({code: ResponseCodes.USER_NOT_FOUND, message: res.errorMsg!});

        return _response.status(HttpCodes.OK).json({code: ResponseCodes.OK, message: 'User found!', data: res.data });
    }

    static async signup(request: e.Request, response: e.Response<IResponseResult>) : Promise<any> {
        const { username, password, email } = request.body;
        const user = new User( username, password, email );

        let res: ObjectId;
        try{
            res = await user.create();
        } catch( error ){
            return response
                .status(HttpCodes.OK)
                .json({code: ResponseCodes.ERROR, message: `${error}`})
        }

        return response
                .status(HttpCodes.OK)
                .json({code: ResponseCodes.OK, message: 'User created successfully', data: `${res}`});
    }


    static index(req: e.Request, res: e.Response) : void {
        res.render('index');
    }
}

export default AuthController;