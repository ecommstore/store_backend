import e from "express";
import debug from 'debug';
import db from '../services/database';
import { HttpCodes, ResponseCodes } from "../helper/http_codes";
import { IResponseResult } from "../types/types";
import { ObjectID } from "mongodb";

const LOG = debug('product:controller');

export default class ProductController {
    
    // [TODO]
    static async getProducts( request: e.Request, response: e.Response ) : Promise<unknown> {
        const products = db.getDB()?.collection('products').find({});
        const result: IResponseResult = { code: 200, message: '', data: null};
        result.data = await products?.toArray();
        // LOG(`'getproducts' ${result.data}`)
        return response.status(HttpCodes.OK).json(result);
    }

    static async getProductById(_req: e.Request, _res: e.Response) : Promise<unknown> {
        const { productId } = _req.body;
        const result: IResponseResult = { code: 200, message: 'OK', data: null};
        try {
            const product = await db.getDB()?.collection('products').findOne({_id: new ObjectID(productId)});
            if(product){
                result.data = product;
                return _res.status(HttpCodes.OK).json('');
            }
            else {
                result.code = ResponseCodes.PRODUCT_NOT_FOUND;
                result.message = 'Product not found';
                return _res.status(ResponseCodes.OK).json(result);
            }
        } catch (error) {
            LOG(`Error getting product by ID : ${error}`);
            result.code = ResponseCodes.ERROR;
            result.message = error;
            return _res.status(HttpCodes.INTERNAL_SERVER_ERROR).json(result);
        }
    }
}