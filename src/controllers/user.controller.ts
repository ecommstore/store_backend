'use strict';
import e from 'express';
import debug from 'debug';
const LOG = debug('user:controller');

class UserController{
    /**
     * Try to login in the system using : email or username and password
     * @param request 
     * @param response 
     */
    static addUser(request: e.Request, response: e.Response) : void {
        LOG('Add user controller');
        // const { params } = request;
        response.render('index');
    }
    
    static getUserById(request: e.Request, response: e. Response) : void {
        response.render('index');
    }
}

export default UserController;